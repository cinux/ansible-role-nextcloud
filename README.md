# nextcloud

Setup Nextcloud and configure the webservice nginx

## Requirements

This role was created while using nginx as webserver and mariadb as database. Therefore you need a webserver, a database and of course php-fpm
You can use [cinux.mariadb](https://codeberg.org/cinux/ansible-role-mariadb), [cinux.nginx](https://codeberg.org/cinux/ansible-role-nginx) and [cinux.php](https://codeberg.org/cinux/ansible-role-php) to solve this issue.

## Role Variables

Please check [defaults/main.yml](defaults/main.yml), [vars/main.yml](vars/main.yml), [molecule/default/prepare.yml](molecule/default/prepare.yml) and [molecule/default/converge.yml](molecule/default/converge.yml)
For other configuration e.g. redis configuration use `molecule/redis` folder

## Dependencies

PHP, Database and Webserver needed.

For testing of this role I used following roles:

- [ansible-role-nginx](https://codeberg.org/cinux/ansible-role-nginx)
- [ansible-role-mariadb](https://codeberg.org/cinux/ansible-role-mariadb)
- [ansible-role-php](https://codeberg.org/cinux/ansible-role-php)
- [ansible-role-redis](https://codeberg.org/cinux/ansible-role-redis) (optional)

## Example Playbook

- hosts: servers
  roles:
  - { role: cinux.nextcloud, cinux_php_fpm_enable: true }

## License

MIT
