# frozen_string_literal: true
# Molecule managed

[
    "php",
    "php-mysql",
    "php-curl",
    "php-cli",
    "php-mysql",
    "php-gd",
    "php-common",
    "php-xml",
    "php-json",
    "php-intl",
    "php-pear",
    "php-imagick",
    "php-dev",
    "php-common",
    "php-mbstring",
    "php-zip",
    "php-soap",
    "php-bz2",
    "php-bcmath",
    "php-gmp",
    "php-apcu",
    "acl",
    "python3-cryptography",
    "unzip"
].each do |pkg|
    describe package(pkg) do
        it { should be_installed }
    end
end

sql = mysql_session('nextcloud','nextcloud')

describe sql.query('show databases like \'nextcloud\';') do
    its('output') { should match(/nextcloud/)}
end

sql = mysql_session('root','root')
[
    "| nextcloud | 127.0.0.1 |",
    "| nextcloud | ::1       |",
    "| nextcloud | localhost |"
].each do |s|
    describe sql.query('SELECT User, Host FROM mysql.user where user = \'nextcloud\';') do
        its('output') { should match(/s/) }
    end
end

# excluded test
# because of following error:
# too short meta escape:
# s = ['<?php',
#     '\$CONFIG = array (',
#     '  \'shared_folder\' => \'\/Mit mir geteilt\',',
#     '  \'trusted_domains\' => array (',
#     '    0 => \'localhost\',',
#     '    1 => \'*\'',
#     '  ),',
#     '  \'memcache.local\' => "\\OC\\Memcache\\Redis",',
#     '  \'memcache.distributed\' => "\\OC\\Memcache\\Redis",',
#     '  \'memcache.locking\' => "\\OC\\Memcache\\Redis",',
#     '  \'redis\' => array(',
#     '    \'host\' => \'127.0.0.1\',',
#     '    \'port\' => 6379,',
#     '  ),',
#     ')',
# ].join("\n")
# describe file('/var/www/nextcloud/config/additional.config.php') do
#     its('content') { should match(s) }
# end


nginx_config = [
    'upstream php-handler {',
    '    #server 127.0.0.1:9000;',
    '    server unix:/var/run/php/php7.4-fpm_nextcloud.sock;',
    '}',
    '',
    'server {',
    '    listen 80;',
    '    listen [::]:80;',
    '    server_name cloud.example.com;',
    '    # enforce https',
    '    return 301 https://$server_name:443$request_uri;',
    '}',
    '',
    'server {',
    '    listen 443 ssl http2;',
    '    listen [::]:443 ssl http2;',
    '    server_name localhost;',
    '',
    '    ssl on;',
    '    ssl_session_cache shared:SSL:10m;',
    '    ssl_protocols TLSv1.2;',
    '    ssl_ciphers EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH;',
    '    ssl_prefer_server_ciphers on;',
    '    ssl_dhparam /etc/nginx/ssl/dhparam.pem;',
    '',
    '    # Use Mozilla\'s guidelines for SSL/TLS settings',
    '    # https://mozilla.github.io/server-side-tls/ssl-config-generator/',
    '    # NOTE: some settings below might be redundant',
    '    ssl_certificate /etc/nginx/ssl/cloud.example.com.crt;',
    '    ssl_certificate_key /etc/nginx/ssl/cloud.example.com.key;',
    '',
    '    # Add headers to serve security related headers',
    '    # Before enabling Strict-Transport-Security headers please read into this',
    '    # topic first.',
    '    add_header Strict-Transport-Security "max-age=15768000; includeSubDomains; preload;" always;',
    '    #',
    '    # WARNING: Only add the preload option once you read about',
    '    # the consequences in https://hstspreload.org/. This option',
    '    # will add the domain to a hardcoded list that is shipped',
    '    # in all major browsers and getting removed from this list',
    '    # could take several months.',
    '    add_header Referrer-Policy "no-referrer" always;',
    '    add_header X-Content-Type-Options "nosniff" always;',
    '    add_header X-Download-Options "noopen" always;',
    '    add_header X-Frame-Options "SAMEORIGIN" always;',
    '    add_header X-Permitted-Cross-Domain-Policies "none" always;',
    '    add_header X-Robots-Tag "none" always;',
    '    add_header X-XSS-Protection "1; mode=block" always;',
    '',
    '    # Remove X-Powered-By, which is an information leak',
    '    fastcgi_hide_header X-Powered-By;',
    '',
    '    # Path to the root of your installation',
    '    root /var/www/nextcloud;',
    '',
    '    location = /robots.txt {',
    '        allow all;',
    '        log_not_found off;',
    '        access_log off;',
    '    }',
    '',
    '    # The following 2 rules are only needed for the user_webfinger app.',
    '    # Uncomment it if you\'re planning to use this app.',
    '    #rewrite ^/.well-known/host-meta /public.php?service=host-meta last;',
    '    #rewrite ^/.well-known/host-meta.json /public.php?service=host-meta-json last;',
    '    location = /.well-known/host-meta {',
    '        return 301 $scheme://$http_hostpublic.php?service=host-meta;',
    '    }',
    '',
    '    location = /.well-known/host-meta.json {',
    '        return 301 $scheme://$http_host/public.php?service=host-meta-json;',
    '    }',
    '',
    '    # The following rule is only needed for the Social app.',
    '    # Uncomment it if you\'re planning to use this app.',
    '    #rewrite ^/.well-known/webfinger /public.php?service=webfinger last;',
    '    location = /.well-known/webfinger {',
    '          return 301 /index.php$uri;',
    '    }',
    '    location = /.well-known/nodeinfo{',
    '        return 301 $scheme://$http_host/index.php/.well-known/nodeinfo;',
    '    }',
    '',
    '    location = /.well-known/carddav {',
    '        return 301 $scheme://$host:$server_port/remote.php/dav;',
    '    }',
    '    location = /.well-known/caldav {',
    '        return 301 $scheme://$host:$server_port/remote.php/dav;',
    '    }',
    '',
    '    # set max upload size',
    '    client_max_body_size 512M;',
    '    fastcgi_buffers 64 4K;',
    '',
    '    # Enable gzip but do not remove ETag headers',
    '    gzip on;',
    '    gzip_vary on;',
    '    gzip_comp_level 4;',
    '    gzip_min_length 256;',
    '    gzip_proxied expired no-cache no-store private no_last_modified no_etag auth;',
    '    gzip_types application/atom+xml application/javascript application/json application/ld+json application/manifest+json application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;',
    '',
    '    # Uncomment if your server is build with the ngx_pagespeed module',
    '    # This module is currently not supported.',
    '    #pagespeed off;',
    '',
    '    location / {',
    '        rewrite ^ /index.php;',
    '    }',
    '',
    '    location ~ ^\/(?:build|tests|config|lib|3rdparty|templates|data)\/ {',
    '        deny all;',
    '    }',
    '    location ~ ^\/(?:\.|autotest|occ|issue|indie|db_|console) {',
    '        deny all;',
    '    }',
    '',
    '    location ~ ^\/(?:index|remote|public|cron|core\/ajax\/update|status|ocs\/v[12]|updater\/.+|oc[ms]-provider\/.+|.+\/richdocumentscode\/proxy)\.php(?:$|\/) {',
    '        fastcgi_split_path_info ^(.+?\.php)(\/.*|)$;',
    '        set $path_info $fastcgi_path_info;',
    '        try_files $fastcgi_script_name =404;',
    '        include fastcgi_params;',
    '        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;',
    '        fastcgi_param PATH_INFO $path_info;',
    '        fastcgi_param HTTPS on;',
    '        # Avoid sending the security headers twice',
    '        fastcgi_param modHeadersAvailable true;',
    '        # Enable pretty urls',
    '        fastcgi_param front_controller_active true;',
    '        fastcgi_pass php-handler;',
    '        fastcgi_intercept_errors on;',
    '        fastcgi_request_buffering off;',
    '    }',
    '',
    '    location ~ ^\/(?:updater|oc[ms]-provider)(?:$|\/) {',
    '        try_files $uri/ =404;',
    '        index index.php;',
    '    }',
    '',
    '    # Adding the cache control header for js, css and map files',
    '    # Make sure it is BELOW the PHP block',
    '    location ~ \.(?:css|js|woff2?|svg|gif|map)$ {',
    '        try_files $uri /index.php$request_uri;',
    '        add_header Cache-Control "public, max-age=15778463";',
    '        # Add headers to serve security related headers (It is intended to',
    '        # have those duplicated to the ones above)',
    '        # Before enabling Strict-Transport-Security headers please read into',
    '        # this topic first.',
    '        #add_header Strict-Transport-Security "max-age=15768000; includeSubDomains; preload;" always;',
    '        #',
    '        # WARNING: Only add the preload option once you read about',
    '        # the consequences in https://hstspreload.org/. This option',
    '        # will add the domain to a hardcoded list that is shipped',
    '        # in all major browsers and getting removed from this list',
    '        # could take several months.',
    '        add_header Referrer-Policy "no-referrer" always;',
    '        add_header X-Content-Type-Options "nosniff" always;',
    '        add_header X-Download-Options "noopen" always;',
    '        add_header X-Frame-Options "SAMEORIGIN" always;',
    '        add_header X-Permitted-Cross-Domain-Policies "none" always;',
    '        add_header X-Robots-Tag "none" always;',
    '        add_header X-XSS-Protection "1; mode=block" always;',
    '',
    '        # Optional: Don\'t log access to assets',
    '        access_log off;',
    '    }',
    '',
    '    location ~ \.(?:png|html|ttf|ico|jpg|jpeg|bcmap|mp4|webm)$ {',
    '        try_files $uri /index.php$request_uri;',
    '        # Optional: Don\'t log access to other assets',
    '        access_log off;',
    '    }',
    '',
    '}'
].join("\n")
describe file('/etc/nginx/sites-enabled/nextcloud.conf') do
    its('content') { should match(nginx_config) }
end

["redis-server", "php-redis"].each do | pkg |
    describe package(pkg) do
        it { should be_installed }
    end
end

redis = ["# ansible managed",
'',
'bind /run/redis/redis-server.sock',
'protected-mode yes',
'port 0',
'tcp-backlog 511',
'timeout 0',
'tcp-keepalive 300',
'daemonize yes',
'supervised no',
'pidfile /var/run/redis/redis-server.pid',
'loglevel notice',
'logfile /var/log/redis/redis-server.log',
'databases 16',
'always-show-logo yes',
'save 900 1',
'save 300 10',
'save 60 10000',
'stop-writes-on-bgsave-error yes',
'rdbcompression yes',
'rdbchecksum yes',
'dbfilename dump.rdb',
'rdb-del-sync-files no',
'dir /var/lib/redis',
'replica-serve-stale-data yes',
'replica-read-only yes',
'repl-diskless-sync no',
'repl-diskless-sync-delay 5',
'repl-diskless-load disabled',
'repl-disable-tcp-nodelay no',
'replica-priority 100',
'acllog-max-len 128',
'lazyfree-lazy-eviction no',
'lazyfree-lazy-expire no',
'lazyfree-lazy-server-del no',
'replica-lazy-flush no',
'lazyfree-lazy-user-del no',
'oom-score-adj no',
'oom-score-adj-values 0 200 800',
'appendonly no',
'appendfilename \"appendonly.aof\"',
'appendfsync everysec',
'no-appendfsync-on-rewrite no',
'auto-aof-rewrite-percentage 100',
'auto-aof-rewrite-min-size 64mb',
'aof-load-truncated yes',
'aof-use-rdb-preamble yes',
'lua-time-limit 5000',
'slowlog-log-slower-than 10000',
'slowlog-max-len 128',
'latency-monitor-threshold 0',
'notify-keyspace-events ""',
'hash-max-ziplist-entries 512',
'hash-max-ziplist-value 64',
'list-max-ziplist-size -2',
'list-compress-depth 0',
'set-max-intset-entries 512',
'zset-max-ziplist-entries 128',
'zset-max-ziplist-value 64',
'hll-sparse-max-bytes 3000',
'stream-node-max-bytes 4096',
'stream-node-max-entries 100',
'activerehashing yes',
'client-output-buffer-limit normal 0 0 0',
'client-output-buffer-limit replica 256mb 64mb 60',
'client-output-buffer-limit pubsub 32mb 8mb 60',
'hz 10',
'dynamic-hz yes',
'aof-rewrite-incremental-fsync yes',
'rdb-save-incremental-fsync yes',
'jemalloc-bg-thread yes',
'redis.session.locking_enabled 1',
'redis.session.lock_retries -1',
'redis.session.lock_wait_time 10000'].join("\n")
describe file('/etc/redis/redis.conf') do
    its('content') { should match(redis) }
end

nextcloud_cron_service = [
    '# Ansible managed',
    '# /etc/systemd/system/nextcloud-cron.service',
    '[Unit]',
    'Description=Nextcloud cron.php job',
    '',
    '[Service]',
    'User=www-data',
    'ExecStart=/usr/bin/php -f /var/www/nextcloud/cron.php',
    '',
    '[Install]',
    'WantedBy=basic.target'
].join("\n")
describe file('/etc/systemd/system/nextcloud-cron.service') do
    its('owner') { should eq 'root'}
    its('group') { should eq 'root'}
    its('mode') { should cmp '0640'}
    its('content') { should match(nextcloud_cron_service) }
end

nextcloud_cron_timer = [
    '# Ansible managed',
    '# /etc/systemd/system/nextcloud-cron.timer',
    '[Unit]',
    'Description=Run Nextcloud cron.php every 5 minutes',
    '',
    '[Timer]',
    'OnBootSec=5min',
    'OnUnitActiveSec=5min',
    'Unit=nextcloud-cron.service',
    '',
    '[Install]',
    'WantedBy=timers.target'
].join("\n")
describe file('/etc/systemd/system/nextcloud-cron.timer') do
    its('owner') { should eq 'root'}
    its('group') { should eq 'root'}
    its('mode') { should cmp '0640'}
    its('content') { should match(nextcloud_cron_timer) }
end

describe service('nextcloud-cron.timer') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
end