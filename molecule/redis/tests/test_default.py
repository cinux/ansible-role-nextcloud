"""Role testing files using testinfra."""

import requests

nextclouddir = "/var/www/nextcloud"

import os
import testinfra.utils.ansible_runner
testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_nextcloud_installed(host):
    """Validate nextcloud is installed."""
    f = host.file(nextclouddir)

    assert f.exists
    assert f.is_directory
    assert f.user == "www-data"
    assert f.group == "www-data"


def test_nextcloud_data_dir_exists(host):
    """Validate nextcloud data dir exists"""
    f = host.file("%s/data"%(nextclouddir))

    assert f.exists
    assert f.is_directory
    assert f.mode == 0o770
    assert f.user == "www-data"
    assert f.group == "www-data"


def test_nextcloud_htaccess(host):
    """Validate nextcloud htaccess"""
    f = host.file("%s/.htaccess"%(nextclouddir))

    assert f.exists
    assert f.is_file
    assert f.mode == 0o644
    assert f.user == "www-data"
    assert f.group == "www-data"
    assert f.contains("")


def test_nextcloud_userini(host):
    """Validate nextcloud user.ini"""
    f = host.file("%s/.user.ini"%(nextclouddir))

    assert f.exists
    assert f.is_file
    assert f.mode == 0o644
    assert f.user == "www-data"
    assert f.group == "www-data"
    assert f.contains("")

import subprocess
def test_webserver_connection(host):
    """ Validate Webserver Responsonse"""

    #requests.get(url,headers={'Authorization': 'GoogleLogin auth=%s' % authorization_token}) 
    #r = requests.get("https://localhost", verify=False)
    # headers = {"Host": "nextcloud"}
    # #r = requests.get("https://localhost", verify=False)
    # url = "https://" + testinfra_hosts[0]
    # print(testinfra_hosts[0])
    # r = requests.get(url, verify=False)
    # print(r.text)
    command = "/usr/bin/curl  --insecure -L https://localhost -I | grep 'HTTP/2'"
    res = subprocess.check_output(command)
    assert "HTTP/2" in res.decode('utf-8')
